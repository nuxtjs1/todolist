import {EditTodo, TodoList, NewTodo} from '@/types/interfaces/todo'

let ID: number = 0;

export const state = () => ({
    todolist: [] as TodoList[]
})

export type RootState = ReturnType<typeof state>;

export const mutations = {
    onStart(state: RootState) {
        const todo: string = localStorage.getItem("todolist")!;
        if(typeof todo === 'string') {
            state.todolist = JSON.parse(todo);
        }
    },
    addTodo(state: RootState, payload: NewTodo) {
        const {title, content} = payload;
        state.todolist = [{id: ID++, title, content, isDone: false}, ...state.todolist]
        localStorage.setItem('todolist', JSON.stringify(state.todolist))
    },
    removeTodo(state: RootState, todo: TodoList) {
        state.todolist.splice(state.todolist.indexOf(todo), 1);
        localStorage.setItem('todolist', JSON.stringify(state.todolist))
    },
    doneTodo(state: RootState, todo: TodoList) {
        state.todolist[state.todolist.indexOf(todo)].isDone = !todo.isDone;
        localStorage.setItem('todolist', JSON.stringify(state.todolist))
    },
    editTodo(state: RootState, payload: EditTodo) {
        const {todo, newTitle, newContent} = payload;
        state.todolist[state.todolist.indexOf(todo)].title = newTitle;
        state.todolist[state.todolist.indexOf(todo)].content = newContent;
        localStorage.setItem('todolist', JSON.stringify(state.todolist))
    }
}

export interface TodoList {
    id: number,
    title: string,
    content: string,
    isDone: boolean
}

export interface NewTodo {
    title: string,
    content: string
}

export interface EditTodo {
    todo: TodoList,
    newTitle: string,
    newContent: string
}

export interface TodoChange {
    title: string,
    content: string,
    isNew: boolean,
    todo?: TodoList
}